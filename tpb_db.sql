-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2022 at 08:03 AM
-- Server version: 10.7.3-MariaDB
-- PHP Version: 8.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `tpb_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key for category',
  `topic` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_rank`
--

CREATE TABLE `category_rank` (
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_rank` int(11) NOT NULL COMMENT 'category by size',
  `day` datetime NOT NULL COMMENT 'day of stats'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `day_leech`
--

CREATE TABLE `day_leech` (
  `doc_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `leech_average` int(11) NOT NULL COMMENT ' average leech of doc per day '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `day_rank`
--

CREATE TABLE `day_rank` (
  `doc_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `day_rank` int(11) NOT NULL COMMENT 'ranking of doc of day'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `day_seed`
--

CREATE TABLE `day_seed` (
  `doc_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `seed_average` int(11) NOT NULL COMMENT 'average seed of doc per day 	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `doc_id` int(11) NOT NULL,
  `doc_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_magnet_link` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploader` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_date_upload` datetime NOT NULL,
  `doc_initial_date` datetime NOT NULL COMMENT 'time of entry in rank',
  `doc_final_date` datetime NOT NULL COMMENT 'last time in rank',
  `doc_rank_duration` int(11) NOT NULL COMMENT 'time in rank',
  `doc_nb_period` int(11) NOT NULL COMMENT 'number of difference sequence in rank with exit in between'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `doc_category`
--

CREATE TABLE `doc_category` (
  `doc_id` int(11) NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `load_data`
--

CREATE TABLE `load_data` (
  `load_id` int(11) NOT NULL,
  `load_date` date NOT NULL,
  `load_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `load_entry`
--

CREATE TABLE `load_entry` (
  `load_id` int(11) NOT NULL,
  `doc_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_magnetlink` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploader` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_date_upload` datetime NOT NULL,
  `doc_size` decimal(10,0) NOT NULL,
  `doc_rank` int(11) NOT NULL,
  `doc_seed` int(11) NOT NULL,
  `doc_leech` int(11) NOT NULL,
  `processed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `search_category`
--

CREATE TABLE `search_category` (
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regex` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uploader_category_stat`
--

CREATE TABLE `uploader_category_stat` (
  `uploader` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'catogory used by uploader',
  `nd_doc` int(11) NOT NULL COMMENT 'number of documents per category'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uploader_stats`
--

CREATE TABLE `uploader_stats` (
  `uploader` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_doc` int(11) NOT NULL COMMENT 'doc stats of uploader',
  `doc_month` int(11) NOT NULL COMMENT 'document stats over a month',
  `doc_year` int(11) NOT NULL COMMENT 'doc stats over a year',
  `nb_category` int(200) NOT NULL COMMENT 'category stats '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `category_rank`
--
ALTER TABLE `category_rank`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `day_leech`
--
ALTER TABLE `day_leech`
  ADD PRIMARY KEY (`doc_id`,`day`);

--
-- Indexes for table `day_rank`
--
ALTER TABLE `day_rank`
  ADD PRIMARY KEY (`doc_id`,`day`);

--
-- Indexes for table `day_seed`
--
ALTER TABLE `day_seed`
  ADD PRIMARY KEY (`doc_id`,`day`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `doc_category`
--
ALTER TABLE `doc_category`
  ADD PRIMARY KEY (`doc_id`,`category`);

--
-- Indexes for table `load_data`
--
ALTER TABLE `load_data`
  ADD PRIMARY KEY (`load_id`) USING BTREE;

--
-- Indexes for table `search_category`
--
ALTER TABLE `search_category`
  ADD PRIMARY KEY (`category`);

--
-- Indexes for table `uploader_category_stat`
--
ALTER TABLE `uploader_category_stat`
  ADD PRIMARY KEY (`category`,`uploader`);

--
-- Indexes for table `uploader_stats`
--
ALTER TABLE `uploader_stats`
  ADD PRIMARY KEY (`uploader`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=539;

--
-- AUTO_INCREMENT for table `load_data`
--
ALTER TABLE `load_data`
  MODIFY `load_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
COMMIT;
