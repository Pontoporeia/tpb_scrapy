import os.path
import zipfile
import mariadb


def tpb_entry(query):
    # needs to be defined to then be changed
    self.error = 0
    # creating connection
    conn_params = {
        "user": "crawler",
        "password": "K]-QHhphOCZb)0kX",
        "host": "localhost",
        "database": "tpb_scraping"
    }
    # db cursor used to input data in DB when scraping
    # self.everything is an object (?) that is shared across defs
    self.connection = mariadb.connect(**conn_params)
    self.cursor = self.connection.cursor()
    # time of epub generation
    try:
        self.cursor.execute(query)
        rows = cur.fetchall()


    except mariadb.Error as err:
        print("MariaDB error at start: {}".format(err))
        self.error = 1
    return(rows)


load_date = datetime.date.today()
load_time_raw = datetime.datetime.now()
load_time = load_time_raw.strftime("%H:%M:%S")

query = f"SELECT * FROM load_entry"
epub = zipfile.ZipFile('tpb_almanach_ '+load_date+'_'+load_time+'.epub', 'w')

# The first file must be named "mimetype"
epub.writestr("mimetype", "application/epub+zip")



tpb_entry(query)

# The filenames of the HTML are listed in html_files
for row in rows:

html_output = """<html><head><title>TPB Sex Study Almanach (plus où moins)</title></head><body><h2>Coucou, voici un output demo de la TPB </h2><table><tr></tr>""""""</body></html>"""

# We need an index file, that lists all other HTML files
# This index file itself is referenced in the META_INF/container.xml
# file
epub.writestr("META-INF/container.xml", '''<container version="1.0"
           xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
  <rootfiles>
    <rootfile full-path="OEBPS/Content.opf" media-type="application/oebps-package+xml"/>
  </rootfiles>
</container>''')

# The index file is another XML file, living per convention
# in OEBPS/Content.xml
index_tpl = '''<package version="2.0"
  xmlns="http://www.idpf.org/2007/opf">
  <metadata/>
  <manifest>
    %(manifest)s
  </manifest>
  <spine toc="ncx">
    %(spine)s
  </spine>
</package>'''

manifest = ""
spine = ""

# Write each HTML file to the ebook, collect information for the index

# manifest += '<item id="file_%s" href="%s" media-type="application/xhtml+xml"/>' % (i+1, basename)
# spine += '<itemref idref="file_%s" />' % (i+1)
epub.write(html_output, 'OEBPS/'+basename)

# Finally, write the index
epub.writestr('OEBPS/Content.opf', index_tpl % {
    'manifest': manifest,
    'spine': spine,
})
