# TPB_scrapy

## Description
Scraping The Pirate Bay's top 100 ebooks, and slowly seing patterns emerge 🍆

Using the **scrapy framework**, tor proxies and mariadb.

## Dependencies
Install through your package manager: 
- `docker`
- `docker-compose`
- `tor`
- `mariadb`
- `python-pip`
	
## Installation
*Archlinux / Manjaro :*

```
sudo pacman -S docker docker-compose tor python-pip mysql
```

Make sure you have `virtualenv` installed: 
```
python -m pip install virtualenv
```

## Setup: 

For LAMP installation, please refer to this [LAMP guide](https://forum.manjaro.org/t/howto-install-apache-mariadb-mysql-php-lamp/13000) from the Manjaro forum. It goes through all the necessary steps to install  Apache, MariaDB, PHP and PHPMyAdmin.

Enable `docker`,`tor` service: 

```
sudo systemctl start docker.service
sudo systemctl enable docker.service
sudo systemctl start tor.service
sudo systemctl enable tor.service
```

Install with `pip` the packages from `requirements.txt`.

```
git clone https://gitlab.com/Zipperflunky/tpb_scrapy.git
```
then:

To install the virtalenv, install the required packages in it: 

```
cd tpb_scrapy
virtualenv venv
source venv/bin/activate
python -m pip install -r requirements.txt
```
Pull docker-compose image and start the tor-proxy containers: 
```
sudo docker-compose pull && sudo docker-compose up -d
```
> Always start the virtualenv with `source venv/bin/activate` before running any python scripts !

## Usage

Usage run this to start crawling: 
```
cd tpb && scrapy crawl tpbspider
```

Use this to run the tpbspider.py without the mariadb pipeline: 

```
cd tpb/spiders && scrapy runspider tpbspider -o output.json
```

## Support
Just open an issue.

## Roadmap
- import and install sql db
- add sql sub routine
- add epub generation for the almanac

## Contributing
Just create an issue that states what you want to modify. I'll review that and we can discuss it there.
Alternatively, you can open a merge request.

## License
TPB_scraper is licensed under GPL 3 and later.

