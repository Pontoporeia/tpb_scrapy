import scrapy
from tpb.items import TpbItem
# regular expression for scraped content parsing
import re
# decoding non-ascii char such as \xa0 or \u00a0
import unicodedata
# to parse and convert date time
from datetime import datetime, timedelta


class TpbspiderSpider(scrapy.Spider):
    name = 'tpbspider'
    allowed_domains = ['https://thepiratebay.zone']
    start_urls = ['https://thepiratebay.zone/top/601']

    def parse(self, response):
        # create items dict
        items = []
        # get every row in the top 100 table
        entry_list = response.css('#searchResult tr:not(.header)')
        # print to differentiate the output from scrapy verbose output
        # print('##################coucou!')
        for i, entry_item in enumerate(entry_list):
            # Get the TpbItem() from items.py
            item = TpbItem()
            # Fill defined scrapy.Fiel() in TpbItem() with the scraped data
            item['doc_name'] = entry_item.css('.detName a::text').get()
            item['uploader']  = entry_item.css('a.detDesc::text').get()
            # info string cleanup, using defs below
            info_raw = entry_item.css('font.detDesc::text').get()
            # cleaning up non-ascii char
            # info_raw=info_raw.decode() --> recreates bytes to string, needed to avoid getting bytes *b'lorem ipsum'*
            # directly unicode.normalize().encode().decode() seems to work, check if works with other methods like re or others.
            info_raw= unicodedata.normalize('NFKD', info_raw).encode('ascii', 'ignore').decode()
            # item['info'] =  info_raw
            date_re = re.findall("(?<=Uploaded ).+?(?=,)", info_raw)
            size_re = re.findall("(?<=Size ).+?(?=,)", info_raw)
            # re.findall returns a bloody list, not a string ... so you need to select  the first item in list, or the one you want if more then one
            # TODO: add fonctions to parse date and size
            item['doc_date_upload'] = timecleanup(date_re[0])
            item['doc_size'] = sizecleanup(size_re[0])     
            item['doc_rank'] = int(i + 1)
            item['doc_seed']  = entry_item.css('td:nth-child(3)::text').get()
            item['doc_leech']  = entry_item.css('td:nth-child(4)::text').get()
            item['doc_magnetlink']  = entry_item.css('.detName+a::attr(href)').get()
            items.append(item)
        #print(items)
        return(items)
# Il faut appeller la fonction en dehors de la class
# function to convert upload times to datetime format %Y-%m-%d %H:%M:%S
def timecleanup(date):
        # convert to today's date
        # Today 00:22
        if re.search("Today", date):
            # get today's date
            date_clean=datetime.today()
            # split by space to get time 
            time_clean=re.split("\s", date)
            # add seconds 
            time_clean_str=time_clean[1]+":00"
            # concatenate both date_clean and time_clean
            upload_datetime=date_clean.strftime("%Y-%m-%d") + " " + time_clean_str
        # convert to today minus - one
        # Y-day 00:38
        elif re.search("Y-day", date):
            # today - one day --> yesterday
            date_clean=datetime.today() - timedelta(days=-1)
            # split by space to get time 
            time_clean=re.split("\s", date)
            # add seconds 
            time_clean_str=time_clean[1]+":00"
            # concatenate both date_clean and time_clean
            upload_datetime=date_clean.strftime("%Y-%m-%d") + " " + time_clean_str
        # add year
        # 03-26 20:59
        elif re.search(":", date):
            datesplit=re.split("\s", date)
            date_clean=datesplit[0] +"-"+ datetime.today().strftime("%Y")
            date_clean= datetime.strptime(date_clean, "%m-%d-%Y").strftime("%Y-%m-%d")
            # Papae est un couillon, on sait po ce qu'on fait...
            # strf to convert to str
            upload_datetime=date_clean+ " "+datesplit[1]+":00"
        else:
            # convert to date and add midnight
            # 11-09 2021
            upload_datetime = datetime.strptime(date, "%m-%d %Y").strftime("%Y-%m-%d") + " 00:00:00"
        return(upload_datetime)

# function that converts the file size scraped from TPB to kilobits
def sizecleanup(size):
    size_clean =  re.split(" ", size)
    # Converting to KiB
    if re.search("KiB", size_clean[1], flags=re.IGNORECASE):
        upload_size = size_clean[0]
    elif re.search("MiB", size_clean[1], flags=re.IGNORECASE):
        size_temp=float(size_clean[0])
        size_temp= size_temp*1024
        upload_size=str(size_temp)
    elif re.search("GiB", size_clean[1], flags=re.IGNORECASE):
        size_temp=float(size_clean[0])
        size_temp= size_temp*1024**2
        upload_size=str(size_temp)
    else:
        upload_size=size_clean[0]
    # change . to , for decimal in DB 
    # upload_size=upload_size.replace(".", ",")
    return(upload_size)