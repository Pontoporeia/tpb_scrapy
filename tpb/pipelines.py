# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import sys
import datetime
import mariadb
from scrapy.exceptions import DropItem
from scrapy.http import Request
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

class TpbPipeline:
    # while spider is open scrapy methode:
    def open_spider(self, spider):
        # needs to be defined to then be changed
        self.error = 0
        # creating connection
        conn_params = {
            "user": "crawler",
            "password": "K]-QHhphOCZb)0kX",
            "host": "localhost",
            "database": "tpb_db"
        }
        # db cursor used to input data in DB when scraping
        # self.everything is an object (?) that is shared across defs
        self.connection = mariadb.connect(**conn_params)
        self.cursor = self.connection.cursor()
        # preparing for load_data table:
        self.load_date = datetime.date.today()
        self.load_time = datetime.datetime.now()
        self.load_date_str = self.load_date.strftime("%Y-%m-%d")
        self.load_time_str = self.load_time.strftime("%H:%M:%S")
        # print(load_date)
        # print(load_time)
        try:
            self.cursor.execute(
                f"INSERT INTO load_data (load_date, load_time) VALUES ('{self.load_date}','{self.load_time}')")
            # keeping last id for next table
            self.load_id = self.cursor.lastrowid
        except mariadb.Error as err:
            print("MariaDB error at start: {}".format(err))
            self.error = 1

    # on spider end :
    def close_spider(self, spider):
        # commiting to DB and closing connection
        # if error, no commit
        if self.error != 1:
            try:
                self.process_load()
            except mariadb.Error as err:
                print("MariaDB error during process_load: {}".format(err))
                self.error = 1
            except :
                print("Other Error during process_load")
                self.error = 1
            if self.error != 1:
                self.connection.commit()
                print("!--------------------------- it worked ---------------------------!")
            else :
                print("Error, during process_load.")
                self.connection.rollback()
        else :
            print("Error before process_load")
            self.connection.rollback()
        self.connection.close()
        

    # on pipeline parsing:
    def process_item(self, item, spider):
        try:
            # for item in items:
            # f before the sql requête pour concatener les variables directement, avec variables
            tmp_doc_name=self.connection.escape_string(item['doc_name'])
            tmp_doc_magnet=self.connection.escape_string(item['doc_magnetlink'])
            self.cursor.execute(
                f"INSERT INTO load_entry (load_id, doc_name, doc_magnetlink, uploader, doc_date_upload, doc_size, doc_rank, doc_seed, doc_leech, processed) VALUES ('{self.load_id}','{tmp_doc_name}','{tmp_doc_magnet}','{item['uploader']}','{item['doc_date_upload']}','{item['doc_size']}','{item['doc_rank']}','{item['doc_seed']}','{item['doc_leech']}', '0')"
                )
            # self.connection.commit()
            # cursor.close()
        except mariadb.Error as err:
            print("MariaDB error on item: {}".format(err))
            self.error = 1


    # inserting or updating secondary data processed from load_data & load_entry
    def process_load(self):
        self.cursor.execute(
            f"SELECT * FROM load_entry WHERE load_id = '{self.load_id}'"
            )
        load_rows=self.cursor.fetchall()
        for load_item in load_rows:
            update_flag = False
            item_doc_name=self.connection.escape_string(load_item[1])
            item_doc_magnet=self.connection.escape_string(load_item[2])
            self.cursor.execute(
                f"SELECT * FROM document WHERE doc_name='{item_doc_name}'"
            )
            doc_item=self.cursor.fetchall()
            if not doc_item:
                # print(load_item)
                self.cursor.execute(
                    f"INSERT INTO document(doc_name,doc_magnet_link, uploader, doc_date_upload, doc_initial_date, doc_final_date, doc_rank_duration, doc_nb_period) VALUES ('{item_doc_name}','{item_doc_magnet}','{load_item[3]}','{load_item[4]}','{self.load_date_str + ' ' + self.load_time_str}','{self.load_date_str + ' ' + self.load_time_str}','{1}','{1}')")
                doc_id = self.cursor.lastrowid
                update_flag = True
                self.cursor.execute(
                    f"INSERT INTO doc_category(doc_id,category) VALUES ('{doc_id}','Undetermined')")
            else:
                diff_date_raw = self.load_date - doc_item[0][6].date()
                diff_date = diff_date_raw.days
                doc_id = doc_item[0][0]
                match diff_date:
                    case diff_date if diff_date == 0: 
                        continue
                    case diff_date if diff_date == 1:
                        self.cursor.execute(
                            f"UPDATE document SET doc_final_date = '{self.load_date_str +' ' + self.load_time_str}', doc_rank_duration = '{doc_item[0][7] + 1}' WHERE doc_id = '{doc_id}'")
                        update_flag = True
                    case diff_date if diff_date >= 1:
                        self.cursor.execute(
                            f"UPDATE document SET doc_final_date = '{self.load_date_str+' '+self.load_time_str}', doc_rank_duration = '{doc_item[0][7]+1}', doc_nb_period = {doc_item[0][8]+1} WHERE doc_id = '{doc_id}'")
                        update_flag = True
                    case diff_date if diff_date <= 0:
                        print("Error, diff_date cannot be inferior to 0, dates are probably muddled")
            # Once document table is filled, day_leech, day_seed and day_rank are updated or created
            if update_flag:
                print("Starting processing because updated flag")
                self.cursor.execute(
                    f"SELECT * FROM load_data d INNER JOIN load_entry e ON d.load_id = e.load_id WHERE d.load_date = '{self.load_date}' AND e.doc_name = '{item_doc_name}' ORDER BY load_date DESC")
                day_load=self.cursor.fetchall()
                if not day_load:
                     print ("Error, empty load ")
                else:
                    day_load_last = day_load[0]
                    print(day_load_last)
                    # TODO: select precise info refactoring
                    # leech_day
                    self.cursor.execute (
                        f"SELECT * FROM day_leech WHERE day = '{self.load_date}' AND doc_id = '{doc_id}'")
                    day_leech = self.cursor.fetchall()
                    if not day_leech:
                        self.cursor.execute (
                            f"INSERT INTO day_leech (doc_id, leech_average, day) VALUES ('{doc_id}','{day_load_last[11]}','{self.load_date}')")
                    else:
                        self.cursor.execute (
                            f"UPDATE day_leech SET leech_average = '{day_load_last[11]}' WHERE doc_id = '{doc_id}' AND day = '{self.load_date}'")
                    # seed_day
                    self.cursor.execute (
                        f"SELECT * FROM day_seed WHERE day = '{self.load_date}' AND doc_id = '{doc_id}'")
                    day_seed = self.cursor.fetchall()
                    if not day_seed:
                        self.cursor.execute (
                            f"INSERT INTO day_seed (doc_id, seed_average, day) VALUES ('{doc_id}','{day_load_last[10]}','{self.load_date}')")
                    else:
                        self.cursor.execute (
                            f"UPDATE day_seed SET seed_average = '{day_load_last[10]}' WHERE doc_id = '{doc_id}' AND day = '{self.load_date}'")
                    #rank_day
                    self.cursor.execute (
                        f"SELECT * FROM day_rank WHERE day = '{self.load_date}' AND doc_id = '{doc_id}'")
                    day_rank = self.cursor.fetchall()
                    if not day_rank:
                        self.cursor.execute (
                            f"INSERT INTO day_rank (doc_id, day_rank, day) VALUES ('{doc_id}','{day_load_last[9]}','{self.load_date}')")
                    else:
                        self.cursor.execute (
                            f"UPDATE day_rank SET day_rank = '{day_load_last[9]}' WHERE doc_id = '{doc_id}' AND day = '{self.load_date}'")
                