# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field

# class load_data(scrapy.Item):
#     # for load_data table
#     # scraping date and time
#     load_date = scrapy.Field()
#     load_time =  scrapy.Field()

class TpbItem(scrapy.Item):
    # for load_entry table
    doc_name =  scrapy.Field()
    doc_magnetlink = scrapy.Field()
    uploader = scrapy.Field()
    doc_date_upload = scrapy.Field()
    doc_size = scrapy.Field()
    doc_rank = scrapy.Field()
    doc_seed = scrapy.Field()
    doc_leech = scrapy.Field()

